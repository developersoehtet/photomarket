from django.http import Http404

from photomarket.mixins import LoginRequiredMixin

#from sellers.mixins import SellerAccountMixin

class ProductManagerMixin(LoginRequiredMixin, object):
	def get_object(self, *args, **kwargs):
		user = self.request.user
		obj = super(ProductManagerMixin, self).get_object(*args, **kwargs)
		try:
			obj.user  == user
		except:
			raise Http404

		if obj.user == user:
			return obj
		else:
			raise Http404
