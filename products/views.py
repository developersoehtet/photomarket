import os;
from mimetypes import guess_type
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponse

from django.conf import settings
from wsgiref.util import FileWrapper

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Product
from django.db.models import Q
from tags.models import Tag
from analytics.models import TagView

from .forms import ProductAddForm, ProductModelForm

from photomarket.mixins import MultiSlugMixin, SubmitBtnMixin, LoginRequiredMixin

from .mixins import ProductManagerMixin
# Create your views here.


from sellers.mixins import SellerAccountMixin


class ProductListView(ListView):
	model = Product
	#template_name = "products/list_view.html"

	def get_queryset(self, *args, **kwargs):
		qs = super(ProductListView, self).get_queryset(**kwargs)
		query = self.request.GET.get("q")
		if query:
			qs = qs.filter(
					Q(title__icontains=query)|
					Q(description__icontains=query)
				).order_by("title")
		return qs


class ProductDetailView(MultiSlugMixin, DetailView):
	model = Product


	def get_context_data(self, *args, **kwargs):
		context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
		obj = self.get_object()
		tags = obj.tag_set.all()
		# rating_avg = obj.productrating_set.aggregate(Avg("rating"), Count("rating"))
		# context["rating_avg"] = rating_avg
		if self.request.user.is_authenticated():
			# rating_obj = ProductRating.objects.filter(user=self.request.user, product=obj)
			# if rating_obj.exists():
			# 	context['my_rating'] = rating_obj.first().rating
			for tag in tags:
				new_view = TagView.objects.add_count(self.request.user, tag)
		return context


class ProductDownloadView(MultiSlugMixin, DetailView):
	model = Product

	def get(self, request, *args, **kwargs):
		obj = self.get_object()
		if obj in request.user.myproducts.products.all():
			filepath = os.path.join(settings.PROTECTED_ROOT, obj.media.path)
			guessed_type = guess_type(filepath)[0]
			wrapper = FileWrapper(open(filepath,'rb'))
			mimetype = 'application/force-download'
			if guessed_type:
				mimetype = guessed_type
				response = HttpResponse(wrapper, content_type=mimetype)

			if not request.GET.get("preview"):
				response["Content-Disposition"] = "attachment; filename=%s" %(obj.media.name)

			response["X-SendFile"] = str(obj.media.name)
			return response
		else:
			raise Http404




class ProductCreateView(LoginRequiredMixin, SubmitBtnMixin, CreateView):
	model = Product
	template_name = "products/form.html"
	form_class = ProductModelForm
	#success_url = "/products/list"
	submit_btn = "Add Product"

	def form_valid(self, form):
		user = self.request.user
		form.instance.user = user
		valid_data = super(ProductCreateView, self).form_valid(form)
		form.instance.managers.add(user)
		tags = form.cleaned_data.get("tags")
		if tags:
			tags_list = tags.split(",")
			for tag in tags_list:
				if not tag == " ":
					new_tag = Tag.objects.get_or_create(title=str(tag).strip())[0]
					new_tag.products.add(form.instance)
		return valid_data


class ProductUpdateView(ProductManagerMixin, SubmitBtnMixin, MultiSlugMixin, UpdateView):
	model = Product
	template_name = "products/form.html"
	form_class = ProductModelForm
	success_url = "/products/list"
	submit_btn = "Update Product"

	def get_initial(self):
		initial = super(ProductUpdateView,self).get_initial()
		tags = self.get_object().tag_set.all()
		initial["tags"] = ", ".join([x.title for x in tags])
		"""
		tag_list = []
		for x in tags:
			tag_list.append(x.title)
		"""
		return initial

	def form_valid(self, form):
		valid_data = super(ProductUpdateView, self).form_valid(form)
		tags = form.cleaned_data.get("tags")
		obj = self.get_object()
		obj.tag_set.clear()
		if tags:
			tags_list = tags.split(",")

			for tag in tags_list:
				if not tag == " ":
					new_tag = Tag.objects.get_or_create(title=str(tag).strip())[0]
					new_tag.products.add(self.get_object())
		return valid_data


def create_view(request):
	print("form here")
	form = ProductAddForm(request.POST or None)
	if form.is_valid():
		print("form valid")
		data = form.cleaned_data
		title = data.get('title')
		desc = data.get('description')
		price = data.get('price')
		prod = Product()
		prod.title = title
		prod.description = desc
		prod.price = price
		prod.save()
	context = {
		'form': form
	}
	return render(request,'products/create_view.html',context)


def update_view(request, pk=None):
	product = get_object_or_404(Product, pk=pk)
	form = ProductModelForm(request.POST or None, instance=product)
	if form.is_valid():
		instance = form.save(commit=False)
		#instance.sale_price = instance.price
		instance.save()
	template = "products/update_view.html"
	context = {
		"object": product,
		"form": form,
		"submit_btn": "Update Product"
		}
	return render(request, template, context)

def detail_view(request, pk=None):
	product = get_object_or_404(Product, id=pk)
	if product is not None:
		context = {
			'object':product
		}
		return render(request,'products/detail_view.html',context)
	else:
		raise Http404

def detail_slug_view(request, slug=None):
	product = get_object_or_404(Product, slug=slug)
	if product is not None:
		context = {
			'object':product
		}
		return render(request,'products/detail_view.html',context)
	else:
		raise Http404


def list_view(request):
	return render(request,'products/list_view.html',{})



class SellerProductListView(SellerAccountMixin, ListView):
	model = Product
	template_name = "sellers/product_list_view.html"

	def get_queryset(self, *args, **kwargs):
		qs = super(SellerProductListView, self).get_queryset(**kwargs)
		qs = qs.filter(seller=self.get_account())
		query = self.request.GET.get("q")
		if query:
			qs = qs.filter(
					Q(title__icontains=query)|
					Q(description__icontains=query)
				).order_by("title")
		return qs
