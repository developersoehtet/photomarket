from django.conf.urls import url
from django.contrib import admin

from .views import (detail_view, detail_slug_view, create_view, update_view,
                    ProductListView, ProductDetailView,
                    ProductDownloadView,
                    ProductCreateView, ProductUpdateView)

urlpatterns = [
    url(r'^update/(?P<pk>\d+)/$', ProductUpdateView.as_view(), name='update_view'),
    url(r'^update/(?P<slug>[\w-]+)/$', ProductUpdateView.as_view(), name='update_slug_view'),
    url(r'^create/$', ProductCreateView.as_view(), name='create_view'),
    url(r'^list/$', ProductListView.as_view(), name='list_view'),
    url(r'^(?P<pk>\d+)/download/$', ProductDownloadView.as_view(), name='download'),
    url(r'^(?P<slug>[\w-]+)/download/$', ProductDownloadView.as_view(), name='download_slug'),
    url(r'^(?P<pk>\d+)/$', ProductDetailView.as_view(), name='detail_view'),
    url(r'^(?P<slug>[\w-]+)/$', ProductDetailView.as_view(), name='detail_slug_view'),
    # url(r'^create/$', TweetCreateView.as_view(), name='create'),
    # url(r'^(?P<pk>\d+)/$', TweetDetailView.as_view(), name='detail'),
    # url(r'^(?P<pk>\d+)/update/$', TweetUpdateView.as_view(), name='update'),
    # url(r'^(?P<pk>\d+)/delete/$', TweetDeleteView.as_view(), name='delete'),
]
