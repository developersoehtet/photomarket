from django.contrib import admin

# Register your models here.
from .models import Product, MyProducts, Thumbnail


class ThumbnailInline(admin.TabularInline):
	extra = 1
	model = Thumbnail

class ProductAdmin(admin.ModelAdmin):
    inlines = [ThumbnailInline]
    list_display = ('__str__','description','price')

    class Mata:
        model = Product

admin.site.register(Product,ProductAdmin)

admin.site.register(Thumbnail)

admin.site.register(MyProducts)
